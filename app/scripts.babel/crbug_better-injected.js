
var headerstohide = 'ReleaseBlock M Component Status OS Pri'.split(' ');

function removeColumns(){
    [...document.querySelectorAll('#resultstablehead th')].filter(e => { 
     var text = e.textContent.replace('▼','').trim();
     var shouldHide = headerstohide.filter(header => header === text).length;
     if (shouldHide) console.log(e);
     return shouldHide;
  })
   .map(e => e.dataset.colIndex)
   .forEach(TKR_toggleColumnUpdate);
}

window.addEventListener('load', setTimeout(removeColumns, 50));
