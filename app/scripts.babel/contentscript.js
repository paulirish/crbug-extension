'use strict';


function injectScript(file, node) {
    var th = document.body;
    var s = document.createElement('script');
    s.setAttribute('src', file);
    th.appendChild(s);
}
injectScript( chrome.extension.getURL('scripts.babel/crbug_better-injected.js'), 'body');


